require "uri"
require "net/http"
require 'json'
# require 'dotenv'
require 'sinatra'
# require 'pry'

class RemoteLockBot < Sinatra::Base

	get '/' do
		"This is the RemoteLock Bot"
	end

	def remotelock_auth()
		# load environment variables
		# Dotenv.load

		# pull in RemoteLock app details
		remotelock_client_id = ENV["REMOTELOCK_CLIENT_ID"]
		remotelock_client_secret = ENV["REMOTELOCK_CLIENT_SECRET"]

		# auth using client credentials
		url = URI("https://connect.remotelock.com/oauth/token")

		https = Net::HTTP.new(url.host, url.port);
		https.use_ssl = true

		request = Net::HTTP::Post.new(url) 
		request["Content-Type"] ="application/x-www-form-urlencoded" 
		request.body ="grant_type=client_credentials&client_id=#{remotelock_client_id}&client_secret=#{remotelock_client_secret}"

		response = https.request(request)
		response = JSON.parse(response.body)

		access_token = response["access_token"]

		# binding.pry
	end

	post '/add-user' do
		# parse client details from incoming request
		client_name = params['Client Name']
		client_email = params['Client Email']

		# get access token
		access_token = remotelock_auth()

		# add authorized lock user
		url = URI("https://api.remotelock.com/access_persons")

		https = Net::HTTP.new(url.host, url.port)
		https.use_ssl = true

		request = Net::HTTP::Post.new(url)
		request["Authorization"] = "Bearer #{access_token}"
		request["Accept"] = "application/vnd.lockstate+json; version=1"
		request["Content-Type"] = "application/json"
		request.body = "{\n    \"type\": \"access_user\",\n    \"attributes\": {\n      \"name\": \"#{client_name}\",\n\"email\": \"#{client_email}\",\n      \"generate_pin\": true\n    }\n  }"

		response = https.request(request)
		response = JSON.parse(response.body)
		remotelock_user_id = response["data"]["id"]

		# binding.pry

		# give new user access to device and access schedule
		device_id = "f6f8ba12-bd76-4536-8f19-2d1684478d33"
		lock_type = "schlage_home_lock"
		access_schedule_id = "11caf695-485d-4a59-9497-2ade50e3c429"


		url = URI("https://api.remotelock.com/access_persons/#{remotelock_user_id}/accesses")

		https = Net::HTTP.new(url.host, url.port)
		https.use_ssl = true

		request = Net::HTTP::Post.new(url)
		request["Authorization"] = "Bearer #{access_token}"
		request["Content-Type"] = "application/json"
		request.body = "{\n  \"attributes\": {\n    \"accessible_id\": \"#{device_id}\",\n    \"accessible_type\": \"#{lock_type}\",\n    \"access_schedule_id\": \"#{access_schedule_id}\"\n  }\n}"


		response = https.request(request)

		# binding.pry

		# send new user email
		url = URI("https://api.remotelock.com/access_persons/#{remotelock_user_id}/email/notify")

		https = Net::HTTP.new(url.host, url.port)
		https.use_ssl = true

		request = Net::HTTP::Post.new(url)
		request["Authorization"] = "Bearer #{access_token}"
		request["Accept"] = "application/vnd.lockstate+json; version=1"
		request["Content-Type"] = "application/json"
		# request.body = "{\n  \"attributes\": {\n    \"days_before\": 1\n  }\n}"

		response = https.request(request)

		# binding.pry
	end

	post '/deactivate-client' do
		# parse client details from incoming request
		client_email = params['Client Email']

		# get access token
		access_token = remotelock_auth()

		# get all users
		url = URI("https://api.remotelock.com/access_persons")

		https = Net::HTTP.new(url.host, url.port)
		https.use_ssl = true

		request = Net::HTTP::Get.new(url)
		request["Authorization"] = "Bearer #{access_token}"

		response = https.request(request)
		response = JSON.parse(response.body)

		# binding.pry

		# parse for user id based upon matching email
		user = response["data"].select { |user| user["attributes"]["email"] == client_email }
		remotelock_user_id = user.first["id"]

		# binding.pry

		# deactivate user
		url = URI("https://api.remotelock.com/access_persons/#{remotelock_user_id}/deactivate")

		https = Net::HTTP.new(url.host, url.port)
		https.use_ssl = true

		request = Net::HTTP::Put.new(url)
		request["Authorization"] = "Bearer #{access_token}"

		response = https.request(request)

		# binding.pry
		# response = JSON.parse(response.body)
	end
end